<?php

/**
 * Declare the available regions implemented by this engine.
 *
 * @return
 *    An array of regions.  The first array element will be used as the default region for themes.
 *    Each array element takes the format: variable_name => t('human readable name')
 */
function celju_regions() {
    return array(
        'sidebar' => t('Sidebar'),
        'content_top' => t('Content Top'),
        'content_bottom' => t('Content Bottom'),
        'footerColumnLeft' =>('Footer Left Column @ 1/3 width'),
        'footerColumnMiddle' =>('Footer Middle Column @ 1/3 width'),
        'footerColumnRight' =>('Footer Right Column @ 1/3 width'),
        'footerContentLeft' =>('Footer Left Column @ 2/3 width'),
        'footerContentRight' =>('Footer Right Column @ 2/3 width')
    );
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
   if (!empty($breadcrumb)) {
     return '<div class="breadcrumb">'. implode(' :: ', $breadcrumb) .'</div>';
   }
 }

/**
 * Different Node Templates by Node Variables (nid, type, view)
 * http://chapterthreellc.com/blog/zirafa/different_node_templates_node_variables_nid_type_view
 */
function _phptemplate_variables($hook, $vars = array()) {
   switch ($hook) {
     case 'node':

       if ($vars['page']) {
         $vars['template_files'] = array('node-default-page', 'node-'. $vars['node']->type .'-page', 'node-'. $vars['node']->nid .'-page');
       }
       else {
         $vars['template_files'] = array('node-'. $vars['node']->nid);
       }
       break;
   }

   return $vars;
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
* Customize a TinyMCE theme.
*
* @param init
*   An array of settings TinyMCE should invoke a theme. You may override any
*   of the TinyMCE settings. Details here:
*
*    http://tinymce.moxiecode.com/wrapper.php?url=tinymce/docs/using.htm
*
* @param textarea_name
*   The name of the textarea TinyMCE wants to enable.
*
* @param theme_name
*   The default tinymce theme name to be enabled for this textarea. The
*   sitewide default is 'simple', but the user may also override this.
*
* @param is_running
*   A boolean flag that identifies id TinyMCE is currently running for this
*   request life cycle. It can be ignored.
*/
function phptemplate_tinymce_theme($init, $textarea_name, $theme_name, $is_running) {

  switch ($textarea_name) {
    // Disable tinymce for these textareas
    case 'log': // book and page log
    case 'img_assist_pages':
    case 'caption': // signature
    case 'pages':
    case 'access_pages': //TinyMCE profile settings.
    case 'user_mail_welcome_body': // user config settings
    case 'user_mail_approval_body': // user config settings
    case 'user_mail_pass_body': // user config settings
    case 'synonyms': // taxonomy terms
    case 'description': // taxonomy terms
      unset($init);
      break;

    // Force the 'simple' theme for some of the smaller textareas.
    case 'signature':
    case 'site_mission':
    case 'site_footer':
    case 'site_offline_message':
    case 'page_help':
    case 'user_registration_help':
    case 'user_picture_guidelines':
      $init['theme'] = 'simple';
      foreach ($init as $k => $v) {
        if (strstr($k, 'theme_advanced_')) unset($init[$k]);
      }
      break;
  }

  // Add some extra features when using the advanced theme.
  // If $init is available, we can extend it
  if (isset($init)) {
    switch ($theme_name) {
     case 'advanced':
   $init['width'] = '100%';
       break;

    }
  }

  // Always return $init
  return $init;
}